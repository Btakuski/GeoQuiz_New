package com.example.btakuski.geoquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

    private Button trueButton;
    private Button falseButton;
    private Button cheatButton;
    private ImageButton nextButton;
    private ImageButton previousButton;
    private TextView questionTextView;
    private TextView cheatTokenTextView;
    private Toast toast;
    private int currentIndex = 0;
    private int currentTokenAmt = 0;
    private float currentScore = 0;
    private boolean isCheater;
    private static final String TAG = "QuizActivity";
    private static final String KEY_INDEX = "index";
    private static final String CHEAT_INDEX = "cheat";
    private static final String TOKEN_INDEX = "token";
    private static final int REQUEST_CODE_CHEAT = 0;
    private static final int MAX_CHEAT_TOKEN_AMOUNT = 3;

    private Question[] questionBank = new Question[] {
            new Question(R.string.question_australia, true),
            new Question(R.string.question_oceans, true),
            new Question(R.string.question_mideast, false),
            new Question(R.string.question_africa, false),
            new Question(R.string.question_americas, true),
            new Question(R.string.question_asia, true),
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        if(savedInstanceState != null) {
            currentIndex = savedInstanceState.getInt(KEY_INDEX, 0);
            isCheater = savedInstanceState.getBoolean(CHEAT_INDEX, false);
            currentTokenAmt = savedInstanceState.getInt(TOKEN_INDEX, 0);
        }

        trueButton = findViewById(R.id.true_button);
        trueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(true);
            }
        });

        falseButton = findViewById(R.id.false_button);
        falseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkAnswer(false);
            }
        });

        cheatButton = findViewById(R.id.cheat_button);


        isCheatTokenMax();
        cheatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentTokenAmt += 1;
                updateTokenTextView();
                boolean answerIsTrue = questionBank[currentIndex].isAnswerTrue();
                Intent intent = CheatActivity.newIntent(QuizActivity.this, answerIsTrue);
                startActivityForResult(intent, REQUEST_CODE_CHEAT);
            }
        });

        nextButton = findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToNext();
                isCheater = false;
            }
        });

        previousButton = findViewById(R.id.previous_button);
        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToPrevious();
            }
        });

        questionTextView = findViewById(R.id.question_text_view);
        questionTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToNext();
                updateQuestion();
            }

        });
        updateQuestion();
    }

    private void updateTokenTextView() {
        cheatTokenTextView = findViewById(R.id.cheat_token_text_view);
        cheatTokenTextView.setText(String.format(
                "Cheat Tokens: %s",  Integer.toString(MAX_CHEAT_TOKEN_AMOUNT - currentTokenAmt))
                );
    }

    private void isCheatTokenMax() {
        if(currentTokenAmt == MAX_CHEAT_TOKEN_AMOUNT) {
            cheatButton.setEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != RESULT_OK) {
            return;
        }

        if(requestCode == REQUEST_CODE_CHEAT) {
            if(data == null) {
                return;
            }
            isCheater = CheatActivity.wasAnswerShown(data);
            if(isCheater) {
                nextButton.setEnabled(false);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(KEY_INDEX, currentIndex);
        savedInstanceState.putBoolean(CHEAT_INDEX, isCheater);
        savedInstanceState.putInt(TOKEN_INDEX, currentTokenAmt);
    }

    private boolean isCurrentAnswerTrue() {
        return questionBank[currentIndex].isAnswerTrue();
    }

    private void moveToPrevious() {
        if(currentIndex > 0) {
            currentIndex = (currentIndex - 1) % questionBank.length;
        } else {
            currentIndex = questionBank.length - 1;
        }
        updateQuestion();
        enableButtons();
    }

    private void moveToNext() {
        currentIndex = (currentIndex + 1) % questionBank.length;
        updateQuestion();
        enableButtons();
    }

    private void updateQuestion() {
        int question = questionBank[currentIndex].getTextResId();
        questionTextView.setText(question);
    }

    public void checkAnswer(boolean userPressedTrue) {
        int messageResourceId;
        boolean answerIsTrue = isCurrentAnswerTrue();

        if(isCheater) {
            messageResourceId = R.string.judgement_toast;
            nextButton.setEnabled(true);
        } else {
            if(userPressedTrue == answerIsTrue) {
                messageResourceId = R.string.correct_toast;
                currentScore += 1;
            } else {
                messageResourceId = R.string.incorrect_toast;
            }
        }
        if(isQuizComplete()) {
            toastScore();
            resetScore();
            resetTokens();
            cheatButton.setEnabled(true);
        }
        disableButtons();
        toastAnswer(messageResourceId);
    }

    private void disableButtons() {
        trueButton.setEnabled(false);
        falseButton.setEnabled(false);
    }

    private void enableButtons() {
        trueButton.setEnabled(true);
        falseButton.setEnabled(true);
    }

    private float calculateScore() {
        return (currentScore / questionBank.length) * 100;
    }

    private void resetScore() {
        currentScore = 0;
    }

    private void resetTokens() {
        currentTokenAmt = 0;
    }

    private boolean isQuizComplete() {
        if(currentIndex == (questionBank.length - 1)) {
            return true;
        }
        return false;
    }

    private void toastAnswer(int messageResourceId) {
        toast = Toast.makeText(QuizActivity.this,
                messageResourceId,
                Toast.LENGTH_SHORT);
        toast.show();
    }

    private void toastScore() {
        toast = Toast.makeText(QuizActivity.this,
                getResources().getString( R.string.final_score)
                        + "\n"
                        + String.format("%.0f%%", calculateScore()),
                Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.show();
    }
}
